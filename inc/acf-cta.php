<?php
/**
 * Created by PhpStorm.
 * User: Richard
 * Date: 06-04-18
 * Time: 11:56
 */

/******* ACF BLOCK - CTA ********/
function register_acf_blocks() {

    // register a testimonial block.
    acf_register_block(array(
        'name'              => 'call-to-action',
        'title'             => __('Call to action'),
        'description'       => __('A personal cta block.'),
        'render_template'   => 'content-block-cta.php',
        'category'          => 'formatting',
        'icon'              => 'admin-comments',
        'keywords'          => array( 'cta' ),
    ));
}

// Check if function exists and hook into setup.
if( function_exists('acf_register_block') ) {
    add_action('acf/init', 'register_acf_blocks');
}

/******* ACF FIELDS - CTA ********/

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
    'key' => 'group_5d01596572418',
    'title' => 'Call to action',
    'fields' => array(
        array(
            'key' => 'field_5d016339cb2ab',
            'label' => 'Call to action gebruiken?',
            'name' => 'call_to_action_gebruiken',
            'type' => 'true_false',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'message' => '',
            'default_value' => 0,
            'ui' => 1,
            'ui_on_text' => '',
            'ui_off_text' => '',
        ),
        array(
            'key' => 'field_5d01597095a11',
            'label' => 'Title',
            'name' => 'title',
            'type' => 'text',
            'instructions' => 'Titel van de call to action',
            'required' => 1,
            'conditional_logic' => array(
                array(
                    array(
                        'field' => 'field_5d016339cb2ab',
                        'operator' => '==',
                        'value' => '1',
                    ),
                ),
            ),
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => 'Klaar voor de volgende stap in jouw communicatie?',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
        array(
            'key' => 'field_5d0159a695a12',
            'label' => 'text',
            'name' => 'text',
            'type' => 'text',
            'instructions' => 'Korte tekst (± 30 tekens)',
            'required' => 1,
            'conditional_logic' => array(
                array(
                    array(
                        'field' => 'field_5d016339cb2ab',
                        'operator' => '==',
                        'value' => '1',
                    ),
                ),
            ),
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => 'Geert helpt je graag verder!',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
        array(
            'key' => 'field_5d0159e295a13',
            'label' => 'Person',
            'name' => 'person',
            'type' => 'radio',
            'instructions' => 'Welk teamlid wordt in de cta gebruikt?',
            'required' => 0,
            'conditional_logic' => array(
                array(
                    array(
                        'field' => 'field_5d016339cb2ab',
                        'operator' => '==',
                        'value' => '1',
                    ),
                ),
            ),
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'choices' => array(
                'Geert' => 'Geert',
                'Karinus' => 'Karinus',
                'Joost' => 'Joost',
            ),
            'allow_null' => 0,
            'other_choice' => 0,
            'default_value' => 'Geert',
            'layout' => 'vertical',
            'return_format' => 'value',
            'save_other_choice' => 0,
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'block',
                'operator' => '==',
                'value' => 'acf/call-to-action',
            ),
        ),
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'page',
            ),
        ),
        array(
            array(
                'param' => 'taxonomy',
                'operator' => '==',
                'value' => 'post_tag',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
));

endif;