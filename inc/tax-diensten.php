<?php
/**
 * Created by PhpStorm.
 * User: Richard
 * Date: 06-04-18
 * Time: 11:56
 */

if ( ! function_exists( 'cp_tax_diensten' ) ) {

// Register Custom Taxonomy
	function cp_tax_diensten() {

		$labels = array(
			'name'                       => _x( 'Diensten tags', 'Taxonomy General Name' ),
			'singular_name'              => _x( 'Diensten tag', 'Taxonomy Singular Name'),
			'menu_name'                  => __( 'Diensten tag' ),
			'all_items'                  => __( 'Alle diensten tags' ),
			'parent_item'                => __( 'Parent Item' ),
			'parent_item_colon'          => __( 'Parent Item:' ),
			'new_item_name'              => __( 'Nieuwe diensten tag' ),
			'add_new_item'               => __( 'Nieuwe diensten tag' ),
			'edit_item'                  => __( 'Edit tag' ),
			'update_item'                => __( 'Update tag' ),
			'view_item'                  => __( 'View tag' ),
			'separate_items_with_commas' => __( 'Separate items with commas' ),
			'add_or_remove_items'        => __( 'Add or remove items'),
			'choose_from_most_used'      => __( 'Choose from the most used' ),
			'popular_items'              => __( 'Popular Items' ),
			'search_items'               => __( 'Search Items' ),
			'not_found'                  => __( 'Not Found' ),
			'no_terms'                   => __( 'No items' ),
			'items_list'                 => __( 'Items list' ),
			'items_list_navigation'      => __( 'Items list navigation' ),
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => false,
		);
		register_taxonomy( 'dienstentag', array( 'post' ), $args );

	}
	add_action( 'init', 'cp_tax_diensten', 0 );

}