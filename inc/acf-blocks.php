<?php
/**
 * Created by PhpStorm.
 * User: Richard
 * Date: 06-04-18
 * Time: 11:56
 */


    // register a sub-titel field
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
    'key' => 'group_5ce94fe37c411',
    'title' => 'Sub-titel',
    'fields' => array(
        array(
            'key' => 'field_subtitle',
            'label' => 'Sub-titel',
            'name' => 'sub-titel',
            'type' => 'text',
            'instructions' => 'De titel die boven de hoofdtitel zichtbaar zal zijn',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'page',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
));

endif;

/////////////////////////////////////////////////////////////////////
// Register case-fields

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
    'key' => 'group_5ce98496db84d',
    'title' => 'Cases velden',
    'fields' => array(
        array(
            'key' => 'field_5ce98755e36fc',
            'label' => 'Logo opdrachtgever',
            'name' => 'logo_opdrachtgever',
            'type' => 'image',
            'instructions' => 'Voeg het logo van de opdrachtgever toe (voorkeur in .svg)',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'return_format' => 'array',
            'preview_size' => 'medium_large',
            'library' => 'all',
            'min_width' => 200,
            'min_height' => 150,
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => 1,
            'mime_types' => '',
        ),
        array(
            'key' => 'field_5ce987b5e36fd',
            'label' => 'Payoff van de case',
            'name' => 'payoff_van_de_case',
            'type' => 'text',
            'instructions' => 'Pakkende zin om het project samen te vatten (±35 woorden).',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'post_category',
                'operator' => '==',
                'value' => 'category:cases',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
));

endif;
